package objstream_test

import (
	"errors"
	"testing"
	"time"

	"codeberg.org/gruf/go-objstream"
)

func TestObjStream(t *testing.T) {
	objs := objstream.New[string](10)

	sync := make(chan any)

	go func(t *testing.T) {
		str := "hello world"
		sync <- str

		objs.TrySend(str)

		err := errors.New("oh no!")
		sync <- err

		objs.SendErr(err)

		sync <- struct{}{}

		if !objs.Close() {
			t.Fatal("failed to close")
		}
	}(t)

	// Receive next result
	res := <-sync

	// Give time for catchup
	time.Sleep(time.Millisecond)

	// Try receive next object
	str, ok, err := objs.TryRecv()
	if !ok {
		t.Fatal("failed to receive")
	} else if res != str {
		t.Fatal("did not received expected object")
	} else if err != nil {
		t.Fatal("received unexpected error")
	}

	// Receive next result
	res = <-sync

	// Give time for catchup
	time.Sleep(time.Millisecond)

	// Try receive next error
	str, ok, err = objs.TryRecv()
	if !ok {
		t.Fatal("failed to receive")
	} else if str != "" {
		t.Fatal("received unexpected object")
	} else if err != res {
		t.Fatal("did not received expected error")
	}

	// Allow goroutine to close
	<-sync

	// Give time for catchup
	time.Sleep(time.Millisecond)

	// Confirm close
	str, ok, err = objs.TryRecv()
	if ok {
		t.Fatal("failed to close")
	} else if str != "" {
		t.Fatal("received unexpected object")
	} else if err != nil {
		t.Fatal("received unexpected error")
	}
}

func TestPipe(t *testing.T) {
	robjs, wobjs := objstream.NewPipe[string](10)

	sync := make(chan any)

	go func(t *testing.T) {
		str := "hello world"
		sync <- str

		wobjs.TrySend(str)

		err := errors.New("oh no!")
		sync <- err

		wobjs.SendErr(err)

		sync <- struct{}{}

		if !wobjs.Close() {
			t.Fatal("failed to close")
		}
	}(t)

	// Receive next result
	res := <-sync

	// Give time for catchup
	time.Sleep(time.Millisecond)

	// Try receive next object
	str, ok, err := robjs.TryRecv()
	if !ok {
		t.Fatal("failed to receive")
	} else if res != str {
		t.Fatal("did not received expected object")
	} else if err != nil {
		t.Fatal("received unexpected error")
	}

	// Receive next result
	res = <-sync

	// Give time for catchup
	time.Sleep(time.Millisecond)

	// Try receive next error
	str, ok, err = robjs.TryRecv()
	if !ok {
		t.Fatal("failed to receive")
	} else if str != "" {
		t.Fatal("received unexpected object")
	} else if err != res {
		t.Fatal("did not received expected error")
	}

	// Allow goroutine to close
	<-sync

	// Give time for catchup
	time.Sleep(time.Millisecond)

	// Confirm close
	str, ok, err = robjs.TryRecv()
	if ok {
		t.Fatal("failed to close")
	} else if str != "" {
		t.Fatal("received unexpected object")
	} else if err != nil {
		t.Fatal("received unexpected error")
	}
}
