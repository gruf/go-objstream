package objstream

import (
	"context"
	"fmt"
	"sync/atomic"
)

// ObjectStream wraps a channel to provide helpful methods for sending
// either object or error types, handling casting and much of the boiler
// plate that would normally be required. It also provides safe closing
// for the channel, i.e. repeat calls to .Close() will not panic.
type ObjectStream[Type any] struct {
	ch chan any
	cl uint32
}

// New returns a new ObjectStream instance with an underlying channel of given capacity.
func New[Type any](cap int) *ObjectStream[Type] {
	return &ObjectStream[Type]{
		ch: make(chan any, cap),
	}
}

// NewPipe creates a new ObjectStream instance returns it cast to both ReadStream and WriteStream, acting as an in-memory pipe.
func NewPipe[Type any](cap int) (*ReadStream[Type], *WriteStream[Type]) {
	objs := New[Type](cap) // alloc new base ObjectStream
	return (*ReadStream[Type])(objs),
		(*WriteStream[Type])(objs)
}

// Open returns whether underlying channel is open.
func (str *ObjectStream[Type]) Open() bool {
	return atomic.LoadUint32(&str.cl) == 0
}

// Send will performing a blocking send of an object type down the stream.
func (str *ObjectStream[Type]) Send(obj Type) {
	str.ch <- obj
}

// SendCtx will perform .Send(), additionally returning error early in the case that context is cancelled.
func (str *ObjectStream[Type]) SendCtx(ctx context.Context, obj Type) error {
	select {
	case <-ctx.Done():
		return ctx.Err()
	case str.ch <- obj:
		return nil
	}
}

// TrySend will perform a non-blocking .Send(), returning false in any case that send is not immediately possible.
func (str *ObjectStream[Type]) TrySend(obj Type) bool {
	select {
	case str.ch <- obj:
		return true
	default:
		return false
	}
}

// SendErr will perform a blocking send of an error type down the stream.
func (str *ObjectStream[Type]) SendErr(err error) {
	str.ch <- err
}

// SendErrCtx will perform .SendErr(), additionally returning error early in the case that context is cancelled.
func (str *ObjectStream[Type]) SendErrCtx(ctx context.Context, err error) error {
	select {
	case <-ctx.Done():
		return ctx.Err()
	case str.ch <- err:
		return nil
	}
}

// TrySendErr will perform a non-blocking .SendErr(), returning false in any case that send is not immediately possible.
func (str *ObjectStream[Type]) TrySendErr(err error) bool {
	select {
	case str.ch <- err:
		return true
	default:
		return false
	}
}

// Recv will block until next send down the stream, returning true with obj / err, or false on close.
func (str *ObjectStream[Type]) Recv() (obj Type, ok bool, err error) {
	var v any

	// Block on next send
	v, ok = <-str.ch

	if !ok {
		// Channel closed
		return
	}

	// Cast received interface
	obj, err = cast[Type](v)
	return
}

// RecvCtx will perform .Recv(), additionally returning error early in the case that context is cancelled.
func (str *ObjectStream[Type]) RecvCtx(ctx context.Context) (obj Type, ok bool, err error) {
	var v any

	select {
	case <-ctx.Done():
		err = ctx.Err()
		return
	case v, ok = <-str.ch:
		if !ok {
			// Channel closed
			return
		}
	}

	// Cast received interface
	obj, err = cast[Type](v)
	return
}

// TryRecv will perform a non-blocking .Recv(), returning false in any case that next object is not instantly available.
func (str *ObjectStream[Type]) TryRecv() (obj Type, ok bool, err error) {
	var v any

	select {
	case v, ok = <-str.ch:
		if !ok {
			// Channel closed
			return
		}
	default:
		return
	}

	// Cast received interface
	obj, err = cast[Type](v)
	return
}

// cast is an internal helper function to cast obj to Type or error.
func cast[Type any](v any) (obj Type, err error) {
	switch v := v.(type) {
	// Object received
	case Type:
		obj = v
		return

	// Error received
	case error:
		err = v
		return

	// Unexpected type
	default:
		err = fmt.Errorf("BUG: unexpected data type: %T", v)
		return
	}
}

// Close will safely close the underlying channel, is a noop if already closed.
func (str *ObjectStream[Type]) Close() bool {
	if atomic.CompareAndSwapUint32(&str.cl, 0, 1) {
		close(str.ch)
		return true
	}
	return false
}

// Len returns the underlying channel length.
func (str *ObjectStream[Type]) Len() int {
	return len(str.ch)
}

// Cap returns the underlying channel capacity.
func (str *ObjectStream[Type]) Cap() int {
	return cap(str.ch)
}
