package objstream

import "context"

// WriteStream is a typedef of ObjectStream exposing only the write methods.
type WriteStream[Type any] ObjectStream[Type]

// Send: see ObjectStream.Send().
func (str *WriteStream[Type]) Send(obj Type) {
	(*ObjectStream[Type])(str).Send(obj)
}

// SendCtx: see ObjectStream.SendCtx().
func (str *WriteStream[Type]) SendCtx(ctx context.Context, obj Type) error {
	return (*ObjectStream[Type])(str).SendCtx(ctx, obj)
}

// TrySend: see ObjectStream.TrySend().
func (str *WriteStream[Type]) TrySend(obj Type) (ok bool) {
	return (*ObjectStream[Type])(str).TrySend(obj)
}

// SendErr: see ObjectStream.SendErr().
func (str *WriteStream[Type]) SendErr(err error) {
	(*ObjectStream[Type])(str).SendErr(err)
}

// SendErrCtx: see ObjectStream.SendErrCtx().
func (str *WriteStream[Type]) SendErrCtx(ctx context.Context, err error) error {
	return (*ObjectStream[Type])(str).SendErrCtx(ctx, err)
}

// TrySendErr: see ObjectStream.TrySendErr().
func (str *WriteStream[Type]) TrySendErr(err error) (ok bool) {
	return (*ObjectStream[Type])(str).TrySendErr(err)
}

// Close: see ObjectStream.Close().
func (str *WriteStream[Type]) Close() bool {
	return (*ObjectStream[Type])(str).Close()
}
