# go-objstream

A simple wrapper around a Go channel with helper methods for streaming object / error types.