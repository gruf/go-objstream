package objstream

import "context"

// ReadStream is a typedef of ObjectStream exposing only the read methods.
type ReadStream[Type any] ObjectStream[Type]

// Recv: see ObjectStream.Recv().
func (str *ReadStream[Type]) Recv() (obj Type, closed bool, err error) {
	return (*ObjectStream[Type])(str).Recv()
}

// RecvCtx: see ObjectStream.RecvCtx().
func (str *ReadStream[Type]) RecvCtx(ctx context.Context) (obj Type, ok bool, err error) {
	return (*ObjectStream[Type])(str).RecvCtx(ctx)
}

// TryRecv: see ObjectStream.TryRecv().
func (str *ReadStream[Type]) TryRecv() (obj Type, ok bool, err error) {
	return (*ObjectStream[Type])(str).TryRecv()
}
